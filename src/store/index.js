import Vue from 'vue'
import Vuex from 'vuex'
import db from '../firebase'
import router from '../router'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        tareas: [],
        tarea: {
            nombre: '',
            id: ''
        }
    },
    mutations: {
        setTareas(state, tareas) {
            state.tareas = tareas;
        },
        setTarea(state, tarea) {
            state.tarea = tarea
        }
    },
    actions: {
        getTareas({
            commit
        }) {
            const tareas = [];
            db.collection('tareas').get()
                .then(snapshot => {
                    snapshot.forEach(doc => {
                        let tarea = doc.data();
                        tarea.id = doc.id;
                        tareas.push(tarea);
                    });
                })
            commit('setTareas', tareas);
        },
        getSingleTarea({
            commit
        }, id) {
            db.collection('tareas').doc(id).get()
                .then(doc => {
                    let tarea = doc.data();
                    tarea.id = doc.id;
                    commit('setTarea', tarea);
                })
        },
        editarTarea({
            commit
        }, tarea) {
            db.collection('tareas').doc(tarea.id).update({
                    nombre: tarea.nombre
                })
                .then(() => {
                    router.push({
                        name: 'inicio'
                    })

                    commit();
                })
        }
    },

    modules: {}
})
