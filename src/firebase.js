import firebase from 'firebase/app'
import 'firebase/firestore'

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyAq5T1saW2xt2JPPh8FO-w-0EIsUYsDjbM",
    authDomain: "crud-example-1ae57.firebaseapp.com",
    databaseURL: "https://crud-example-1ae57.firebaseio.com",
    projectId: "crud-example-1ae57",
    storageBucket: "crud-example-1ae57.appspot.com",
    messagingSenderId: "874347641517",
    appId: "1:874347641517:web:d957c1666280ee68f66424",
    measurementId: "G-WTRHSJY2MF"
};

// Initialize Firebase
const firebaseApp = firebase.initializeApp(firebaseConfig);

export default firebaseApp.firestore()
